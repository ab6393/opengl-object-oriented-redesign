#version 150

const int MAX_LIGHTS = 8;

// Phong fragment shader
//
// Author: Andrew Baumher

//Uses a modified version of the code available publicly on wikibooks.
//https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Multiple_Lights


// INCOMING DATA
varying vec3 vertex;
varying vec3 normals;

//all the lights
//currently only supports a directional, point, or ambient light
uniform vec4[MAX_LIGHTS] lightIntensity;
uniform vec3[MAX_LIGHTS] attenuation;
uniform vec4[MAX_LIGHTS] lightData;
uniform int numLights;

//object
uniform vec4 matColor;
uniform vec4 matDiffuse;
uniform vec4 matSpecular;

uniform float objAmbience;
uniform float objDiffuse;
uniform float objSpecular;
uniform float objShininess;

// OUTGOING DATA
out vec4 fragmentColor;

void main()
{
	
	//light related vectors
	vec4 totalLight = vec4(0.0, 0.0, 0.0, 1.0);
	for (int i = 0; i<numLights;i++)
	{
		if (lightData[i].w == -1.0)
		{
			totalLight += lightIntensity[i] * matColor * objAmbience;
		}
		else
		{
			vec3 incident;
			float atten;
			if (lightData[i].w == 0.0)
			{
				atten = 1;
				incident = lightData[i].xyz;
			}
			else
			{
				incident = normalize(lightData[i].xyz - vertex);
				float distance = length(lightData[i].xyz - vertex);
				atten = 1.0 / (attenuation[i].x + attenuation[i].y * 
					distance + attenuation[i].z * pow (distance, 2.0));			
			}
			vec3 reflection = normalize(-reflect(incident, normals));
			vec3 viewpoint = normalize(-vertex);

			//make the dp at min 0.
			float dp = max(dot(normals, incident), 0.0);
			//then find the diffuse
			vec4 diffuse = lightIntensity[i] * objDiffuse * matDiffuse *
				dp;

			//same as above
			dp = max(dot(reflection, viewpoint), 0.0);
			//and the specular
			vec4 specular = lightIntensity[i] * objSpecular * matSpecular * 
				pow(dp, objShininess);

			totalLight += atten * (diffuse + specular);
		}
	}

	fragmentColor = clamp(totalLight, 0.0, 1.0);
}