
/**
 * Vec3.java
 * Abstract vec3 class. most things that inherit this are glorified typedefs
 * @author Andrew Baumher
 */

public abstract class Vec3
{
	
	public static final int NUM_ELEMS = 3;
	
	public float x;
	public float y;
	public float z;

	///
	// Constructor
	//
	// @param xc x coordinate of this point
	// @param yc y coordinate of this point
	// @param zc z coordinate of this point
	///
	public Vec3(float xc, float yc, float zc)
	{
		this.x = xc;
		this.y = yc;
		this.z = zc;
	}
	
	public void update(float xc, float yc, float zc)
	{
		this.x = xc;
		this.y = yc;
		this.z = zc;
	}
	
	public float[] asArray()
	{
		return new float[]
		{
			x, y, z
		};
	}

	/*
	 * Helper function that finds the dot product between two vertices/vectors
	 * @param eye eyepoint vertex
	 * @param axis axis to compare it to. likely a vector
	 * @return the dot product
	 */
	public float dotProduct(Vec3 other)
	{
		return x * other.x + y * other.y + z * other.z;
	}
}

class Vertex extends Vec3
{
	public Vertex(float xc, float yc, float zc)
	{
		super(xc, yc, zc);
	}
}

class Vector extends Vec3
{
	public Vector(float xc, float yc, float zc)
	{
		super(xc, yc, zc);
	}

	/**
	 * Finds the difference vector between two vec3s (generally vertexes) and normalizes it
	 * @param begin
	 * @param end 
	 */
	public Vector(Vec3 begin, Vec3 end)
	{
		super(begin.x - end.x, begin.y - end.y, begin.z - end.z);

		Normalize();
	}

	/**
	 * Normalizes the vector
	 */
	public final void Normalize()
	{
		double length = Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0) + Math.pow(z, 2.0));
		x /= length;
		y /= length;
		z /= length;
	}

	/**
	 * Finds the cross product with another vec3
	 * @param other
	 * @return 
	 */
	Vector crossProduct(Vec3 other)
	{
		Vector retVal = new Vector(0.0f, 0.0f, 0.0f);
		retVal.x = y * other.z - z * other.y;
		retVal.y = z * other.x - x * other.z;
		retVal.z = x * other.y - y * other.x;

		retVal.Normalize();
		return retVal;
	}
}

class Magnitude extends Vec3
{
	public Magnitude(float xc, float yc, float zc)
	{
		super(xc, yc, zc);
	}

}

//not normalized, because normal mapping can use the magnitude of a normal.
class Normal extends Vec3
{
	public Normal(float xc, float yc, float zc)
	{
		super(xc, yc, zc);
	}
	
}

