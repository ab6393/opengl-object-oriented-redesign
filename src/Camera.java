
import javax.media.opengl.GL3;

/**
 * Camera.java
 * Contains all camera related stuff
 * @author Andrew Baumher
 */
public class Camera
{
	/**
	 * Enumeration for type of projection
	 */
	public static enum ProjectionType
	{
		ORTHOGRAPHIC, FRUSTUM
	};
	float near, far, left, right, top, bottom;

	Vertex position;
	Vector up;
	Vertex lookat;
	ProjectionType projection;

	/**
	 * Contructor
	 * @param pos Vertex the camera is located at
	 * @param upVec Up vector
	 * @param look Lookat point for camera
	 * @param type type of projection
	 */
	Camera(Vertex pos, Vector upVec, Vertex look, ProjectionType type)
	{
		position = pos;
		up = upVec;
		lookat = look;
		projection = type;
	}

	/**
	 * A static method that when procced returns a new camera in the default position
	 * @return the default camera
	 */
	public static Camera DefaultCamera()
	{
		Vertex pos = new Vertex(1, 1, 1);
		Vector upVec = new Vector(0, 1, 0);
		Vertex look = new Vertex(0, 0, 0);
		return new Camera(pos, upVec, look, Camera.ProjectionType.FRUSTUM);
	}
	
	/**
	 * Moves the camera w/o removing it
	 * @param pos new position
	 * @param upVec new up vector
	 * @param look new lookat position
	 */
	void repositionCamera(Vertex pos, Vector upVec, Vertex look)
	{
		position = pos;
		up = upVec;
		lookat = look;
	}
	
	/**
	 * Changes the projection type. technically not necessary b/c i dont have proper
	 * security setup (yet)
	 * @param type 
	 */
	public void changeProjection (ProjectionType type)
	{
		projection = type;
	}
	
	/**
	 * Pass the variables for projection in via this call
	 * @param nr near
	 * @param fr far
	 * @param lf left
	 * @param rt right
	 * @param tp top
	 * @param bot bottom
	 */
	public void initializeProjection(float nr, float fr, float lf, float rt, float tp, float bot)
	{
		near = nr;
		far = fr;
		left = lf;
		right = rt;
		top = tp;
		bottom = bot;
	}
	
	/**
	 * Passes the variables in to the shader program for camera positions
	 * @param gl3
	 * @param program 
	 */
	void setupCamera(GL3 gl3, int program)
	{
		gl3.glUseProgram(program);
		int shaderVar = gl3.glGetUniformLocation(program, "projection");
		gl3.glUniformMatrix4fv(shaderVar, 1, true, setupProjection(), 0);
		shaderVar = gl3.glGetUniformLocation(program, "camera"); 
		//determine the nuv vectors t is the translate
		Vector n, u, v, t;

		//find n, u, v, and the transpose, which i call t
		n = new Vector(position, lookat);
		u = up.crossProduct(n);
		v = n.crossProduct(u);
		//t is a magnitude (so technically not a vector)
		//of the three dot products of eye and u/v/n. 
		t = dotProductGroup(position, u, v, n);

		//again, matrix in logical order. transposed when sent to shader
		float[] camera =
		{
			 u.x,  u.y,  u.z, -t.x,
			 v.x,  v.y,  v.z, -t.y,
			 n.x,  n.y,  n.z, -t.z,
			 0.0f, 0.0f, 0.0f, 1.0f
		};
		gl3.glUniformMatrix4fv(shaderVar, 1, true, camera, 0);
	}
	
	private float[] setupProjection()
	{
		switch (projection)
		{
			case ORTHOGRAPHIC:
				return new float[]
				{
					2 / (right - left), 0.0f, 0.0f, (right + left) / (left - right),
					0.0f, 2 / (top - bottom), 0.0f, (top + bottom) / (bottom - top),
					0.0f, 0.0f, 2 / (near - far), (far + near) / (near - far),
					0.0f, 0.0f, 0.0f, 1.0f
				};
			case FRUSTUM:
			default:
				return new float[]
				{
					2.0f * near / (right - left), 0.0f, (right + left) / (right - left), 0.0f,
					0.0f, 2.0f * near / (top - bottom), (top + bottom) / (top - bottom), 0.0f,
					0.0f, 0.0f, (far + near) / (near - far), (2 * far * near) / (near - far),
					0.0f, 0.0f, -1.0f, 0.0f
				};
		}
	}

	/**
	 * Helper function. finds dot products of eyepoint and u/v/n, and maps
	 * them to x/y/z respectively, of a new vector
	 * @param eye eyepoint coordinate
	 * @param u u vector
	 * @param v v vector
	 * @param n n vector
	 * @return vector of the three dot products
	 */
	private Vector dotProductGroup(Vec3 eye, Vec3 u, Vec3 v, Vec3 n)
	{
		float x, y, z;
		x = eye.dotProduct(u);
		y = eye.dotProduct(v);
		z = eye.dotProduct(n);

		return new Vector(x, y, z);
	}
}
