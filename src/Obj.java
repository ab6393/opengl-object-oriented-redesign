
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.GL;
import javax.media.opengl.GL3;

/**
 * Obj.java
 * A combination of original code and reworking of code in the canvas, main, and bufferset
 * @author Warren Carithers
 * @author Andrew Baumher
 */
/**
 * Reads and stores data on objects. Stores an object with all its vertices
 * elements, normals, etc. then, to prepare for drawing, it finds buffers and
 * passes them to OpenGL. also provides a function to initialize and start
 * drawing the object.
 *
 * @author Andrew Baumher
 */
public class Obj
{
	///
	// canvas size information
	///
	private int vbuffer;         // buffer handles
	private int ebuffer;
	private int numElements;     // total number of vertices
	private long vSize;          // component sizes (bytes)
	private long nSize;
	private long tSize;
	private long eSize;
	private boolean hasUVS;

	private final String fileLocation;

	private final ArrayList<Float> normals;
	private final ArrayList<Float> points;
	private final ArrayList<Integer> elements;
	private final ArrayList<Float> uv;
	
	private final String MATERIALS_LOC = "Materials" + File.separator;
	///
	// Constructor
	///
	public Obj(String filename)
	{
		vbuffer = 0;
		ebuffer = 0;
		numElements = 0;
		vSize = 0;
		nSize = 0;
		tSize = 0;
		eSize = 0;

		points = new ArrayList<>();
		normals = new ArrayList<>();
		elements= new ArrayList<>();
		uv= new ArrayList<>();
		
		fileLocation = filename;
		readFile(fileLocation);
	}

	public void dump()
	{
		System.out.print(" vb " + vbuffer);
		System.out.print(" eb " + ebuffer);
		System.out.print(" #E " + numElements);
		System.out.print(" vS " + vSize);
		System.out.print(" nS " + nSize);
		System.out.print(" tS " + tSize);
		System.out.print(" eS " + eSize);
	}
	
	/**
	 * Reads an obj file and parses it into GL ready data
	 * @param filename the file to parse
	 */
	private void readFile(String filename)
	{
		ArrayList<Vertex> vertices = new ArrayList<>();
		ArrayList<Normal> normal = new ArrayList<>();
		ArrayList<UVcoord> coordinates = new ArrayList<>();

		//fileIO here. note that it expects the files to be in a Materials folder
		try (BufferedReader reader = new BufferedReader(new FileReader(MATERIALS_LOC + filename)))
		{
			String line;
			while ((line = reader.readLine()) != null)
			{
				//tokenize the string so each bit is usable and easily parsed to an int
				String[] tokens = line.split("\\s|/");
				if (tokens[0].equalsIgnoreCase("v"))
				{
					float x = Float.parseFloat(tokens[1]);
					float y = Float.parseFloat(tokens[2]);
					float z = Float.parseFloat(tokens[3]);
					vertices.add(new Vertex(x, y, z));
				}
				else if (tokens[0].equalsIgnoreCase("vt"))
				{
					float u = Float.parseFloat(tokens[1]);
					float v = Float.parseFloat(tokens[2]);
					coordinates.add(new UVcoord(u, v));
				}
				else if (tokens[0].equalsIgnoreCase("vn"))
				{
					float x = Float.parseFloat(tokens[1]);
					float y = Float.parseFloat(tokens[2]);
					float z = Float.parseFloat(tokens[3]);
					normal.add(new Normal(x, y, z));
				}
				else if (tokens[0].equalsIgnoreCase("f"))
				{
					int[] locs = new int[9];

					boolean parsedUVs = !tokens[1].equals("");

					for (int q = 1; q < tokens.length; q++)
					{
						if (parsedUVs || q % 3 != 2)
						{
							locs[q - 1] = Integer.parseInt(tokens[q]) - 1;
						}
					}
					
					if (parsedUVs)
					{
						addTriangleWithNormAndUV(vertices.get(locs[0]), normal.get(locs[2]),
							coordinates.get(locs[1]), vertices.get(locs[3]), normal.get(locs[5]),
							coordinates.get(locs[4]), vertices.get(locs[6]), normal.get(locs[8]),
							coordinates.get(locs[7]));
					}
					else
					{
						addTriangleWithNorms(vertices.get(locs[0]), normal.get(locs[2]),
							vertices.get(locs[3]), normal.get(locs[5]), vertices.get(locs[6]),
							normal.get(locs[8]));
					}
					
				}
			}
		}
		catch (IOException ex)
		{
			Logger.getLogger(Obj.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}
	}

	private void addTriangleWithNormAndUV(Vertex v0, Normal n0, UVcoord uv0,
		Vertex v1, Normal n1, UVcoord uv1, Vertex v2, Normal n2, UVcoord uv2)
	{
		addTriangleWithNorms(v0, n0, v1, n1, v2, n2);
		uv.add(uv0.u);
		uv.add(uv0.v);
		uv.add(uv1.u);
		uv.add(uv1.v);
		uv.add(uv2.u);
		uv.add(uv2.v);
	}

	///
	// adds a triangle to the current shape, along with (u,v) data
	//
	// @param p0 first triangle vertex
	// @param uv0 first vertex (u,v) data
	// @param p1 second triangle vertex
	// @param uv1 second vertex (u,v) data
	// @param p2 final triangle vertex
	// @param uv2 final vertex (u,v) data
	///
	private void addTriangleWithUV(Vertex p0, UVcoord uv0,
		Vertex p1, UVcoord uv1, Vertex p2, UVcoord uv2)
	{
		// calculate the normal
		float ux = p1.x - p0.x;
		float uy = p1.y - p0.y;
		float uz = p1.z - p0.z;

		float vx = p2.x - p0.x;
		float vy = p2.y - p0.y;
		float vz = p2.z - p0.z;

		Normal nn = new Normal((uy * vz) - (uz * vy),
			(uz * vx) - (ux * vz),
			(ux * vy) - (uy * vx));

		// Attach the normal to all 3 vertices
		addTriangleWithNorms(p0, nn, p1, nn, p2, nn);

		// Attach the texture coordinates
		uv.add(uv0.u);
		uv.add(uv0.v);
		uv.add(uv1.u);
		uv.add(uv1.v);
		uv.add(uv2.u);
		uv.add(uv2.v);
	}

	///
	// adds a triangle to the current shape, along with normal data
	//
	// @param p0 first triangle vertex
	// @param n0 first triangle normal data
	// @param p1 second triangle vertex
	// @param n1 second triangle normal data
	// @param p2 final triangle vertex
	// @param n2 final triangle normal data
	///
	private void addTriangleWithNorms(Vertex p0, Normal n0,
		Vertex p1, Normal n1, Vertex p2, Normal n2)
	{
		points.add(p0.x);
		points.add(p0.y);
		points.add(p0.z);
		points.add(1.0f);
		elements.add(numElements++);

		normals.add(n0.x);
		normals.add(n0.y);
		normals.add(n0.z);

		points.add(p1.x);
		points.add(p1.y);
		points.add(p1.z);
		points.add(1.0f);
		elements.add(numElements++);

		normals.add(n1.x);
		normals.add(n1.y);
		normals.add(n1.z);

		points.add(p2.x);
		points.add(p2.y);
		points.add(p2.z);
		points.add(1.0f);
		elements.add(numElements++);

		normals.add(n2.x);
		normals.add(n2.y);
		normals.add(n2.z);
	}

	///
	// get the array of vertices for the current shape
	///
	private Buffer getVertices()
	{
		float v[] = new float[points.size()];
		for (int i = 0; i < points.size(); i++)
		{
			v[i] = (points.get(i));
		}
		return FloatBuffer.wrap(v);
	}

	///
	// get the array of normals for the current shape
	///
	private Buffer getNormals()
	{
		float n[] = new float[normals.size()];
		for (int i = 0; i < normals.size(); i++)
		{
			n[i] = (normals.get(i));
		}
		return FloatBuffer.wrap(n);
	}

	///
	// get the array of texture coords for the current shape
	///
	private Buffer getUV()
	{
		float t[] = new float[uv.size()];
		for (int i = 0; i < uv.size(); i++)
		{
			t[i] = (uv.get(i));
		}
		return FloatBuffer.wrap(t);
	}

	///
	// get the array of elements for the current shape
	///
	private Buffer getElements()
	{
		int e[] = new int[elements.size()];
		for (int i = 0; i < elements.size(); i++)
		{
			e[i] = (elements.get(i));
		}
		return IntBuffer.wrap(e);
	}

	private void createBuffers(GL3 gl3)
	{
		Buffer pts = getVertices();
		vSize = numElements * Vec4.NUM_ELEMS * Float.BYTES;
		long vbufferSize = vSize;

		Buffer norms = getNormals();
		nSize = numElements * Vec3.NUM_ELEMS * Float.BYTES;
		vbufferSize += nSize;

		Buffer uvs = getUV();
		if (uv != null)
		{
			tSize = numElements * 2l * Float.BYTES;
		}
		vbufferSize += tSize;

		Buffer elems = getElements();
		eSize = numElements * Integer.BYTES;

		ebuffer = makeBuffer(gl3, GL.GL_ELEMENT_ARRAY_BUFFER,
			elems, eSize);

		// next, the vertex buffer, containing vertices and "extra" data
		vbuffer = makeBuffer(gl3, GL.GL_ARRAY_BUFFER, null, vbufferSize);
		gl3.glBufferSubData(GL.GL_ARRAY_BUFFER, 0, vSize, pts);
		gl3.glBufferSubData(GL.GL_ARRAY_BUFFER, vSize, nSize, norms);
		if (uv != null)
		{
			gl3.glBufferSubData(GL.GL_ARRAY_BUFFER, vSize + nSize,
				tSize, uvs);
		}
	}

	private int makeBuffer(GL3 gl3, int target, Buffer data, long size)
	{
		int buffer[] = new int[1];

		//generates a buffer in opengl for every element
		//of the buffer array. the id of this buffer is stored
		//at that index in the array.
		gl3.glGenBuffers(1, buffer, 0);
		//we then bind the type of data to be sent to that buffer
		//to the buffer it is being sent to
		gl3.glBindBuffer(target, buffer[0]);
		//and then we fill that data. we let opengl know that
		//this data will be written once, but read many times, and 
		//it will be used for drawing. 
		gl3.glBufferData(target, size, data, GL.GL_STATIC_DRAW);
		//return id of this buffer
		return (buffer[0]);
	}

	/**
	 * Draws the object. Should never be called directly from main. Instead,
	 * each shape should call this when the shape has been fully initialized
	 * (i.e. lights, model transforms, camera have been set up and ready for
	 * drawing)
	 *
	 * @param gl3
	 * @param program
	 */
	public void drawObject(GL3 gl3, int program)
	{
		gl3.glUseProgram(program);
		createBuffers(gl3);
	//	gl3.glGetProgramiv(program, GL3.GL_ACTIVE_ATTRIBUTES, ib);
		gl3.glBindBuffer(GL.GL_ARRAY_BUFFER, vbuffer);
		gl3.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ebuffer);

		// set up the vertex attribute variables
		int vPosition = gl3.glGetAttribLocation(program, "vPosition");
		gl3.glEnableVertexAttribArray(vPosition);
		gl3.glVertexAttribPointer(vPosition, Vec4.NUM_ELEMS, GL.GL_FLOAT, false,
			0, 0l);

		int vNormal = gl3.glGetAttribLocation(program, "vNormal");
		gl3.glEnableVertexAttribArray(vNormal);
		gl3.glVertexAttribPointer(vNormal, Vec3.NUM_ELEMS, GL.GL_FLOAT, false,
			0, vSize);

		if (hasUVS)
		{
			int vTexCoord = gl3.glGetAttribLocation(program, "vTexCoord");
			gl3.glEnableVertexAttribArray(vTexCoord);
			gl3.glVertexAttribPointer(vTexCoord, UVcoord.NUM_ELEMS, GL.GL_FLOAT, false,
				0, vSize + nSize);
		}

		gl3.glDrawElements(GL.GL_TRIANGLES, (int) vSize, GL.GL_UNSIGNED_INT, 0l
		);
	}
}
