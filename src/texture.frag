#version 150

// Texture mapping vertex shader
//
// Contributor:  Andrew Baumher

const int MAX_LIGHTS = 8;

// INCOMING DATA
varying vec3 vertex;
varying vec3 normals;
varying vec2 texCoord;

uniform vec4[MAX_LIGHTS] lightIntensity;
uniform vec3[MAX_LIGHTS] attenuation;
uniform vec4[MAX_LIGHTS] lightData;
uniform int numLights;
//things such as the skybox wouldn't be affected by lighting.
//i suppose if you wanted to be more uniform, the material could emit the color
//of the object, but have near immediate falloff.
uniform bool affectedByLights;

uniform sampler2D tex;

uniform float objAmbience;
uniform float objDiffuse;
uniform float objSpecular;
uniform float objShininess;

// OUTGOING DATA
out vec4 fragmentColor;

//diffuse and specular are essentially omitted. see readme.
void main()
{
	vec4 matColor = texture(tex, texCoord);
	if (affectedByLights)
	{
		vec4 totalLight = vec4(0.0, 0.0, 0.0, 1.0);
		for (int i = 0; i<numLights;i++)
		{
			if (lightData[i].w == -1.0)
			{
				totalLight += lightIntensity[i] * matColor * objAmbience;
			}
			else
			{
				vec3 incident;
				float atten;
				if (lightData[i].w == 0.0)
				{
					atten = 1;
					incident = lightData[i].xyz;
				}
				else
				{
					incident = normalize(lightData[i].xyz - vertex);
					float distance = length(lightData[i].xyz - vertex);
					atten = 1.0 / (attenuation[i].x + attenuation[i].y * 
						distance + attenuation[i].z * pow (distance, 2.0));			
				}
				vec3 reflection = normalize(-reflect(incident, normals));
				vec3 viewpoint = normalize(-vertex);

				//make the dp at min 0.
				float dp = max(dot(normals, incident), 0.0);
				//then find the diffuse
				vec4 diffuse = lightIntensity[i] * objDiffuse * matColor * dp;

				//same as above
				dp = max(dot(reflection, viewpoint), 0.0);
				//and the specular
				vec4 specular = lightIntensity[i] * objSpecular * matColor * 
					pow(dp, objShininess);

				totalLight += atten * (diffuse + specular);
			}
		}
		fragmentColor = clamp(totalLight, 0.0, 1.0);
	}
	else
	{
		fragmentColor = matColor;
	}
}