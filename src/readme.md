This project is a final project for Computer Graphics in OpenGL. The code base was originally written in C, then ported to C++, and then finally JAVA. As such, most of the program was designed in such a way that doesnt take advantage of OOP. At the time of writing, the professor's view on the code was more about function than form, as maintaining all 3 versions was already a tedious process. This is my take on a cleaner, more maintainable version that also greatly benefits from Object-Oriented Paradigms. 

The end goal is a backend that takes care of the setup and rendering. the end user simply loads the textures and models they would like to use, and then adds them to the scene. changing position, scale, rotation, etc. is a simple process of changing the properties. similarly, changing the camera to orthographic or frustum is provided, and again, the user can set the properties (near, far, etc) as needed.

This allows the backend to be potentially complex, but hidden from the average user. this makes the frontend maintainable and future proof, because all it does is call functions that do the work. modular design such as this would also work in an education setting: remove all the code from the implementation of thedirectional light, for example, and have the students implement it.

Other Design Changes:
Move from doing Uniform calculations on the GPU (aka the same calculation for every point) to the CPU (just once)
Memory allocation cleanup (especially with the buffers) - data that can be stored once and reused is better than
allocating and populating it each time its needed. Saves cycles, and actually uses less memory (if you have multiple instances of a polygon, for example, it only allocates the vertex buffer once)
General Code cleanup (ex: Vector is deprecated. Arraylist is the replacement). 