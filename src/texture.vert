#version 150

// Texture mapping vertex shader
//
// Contributor:  Andrew Baumher

// INCOMING DATA

// Vertex location (in model space)
in vec4 vPosition;

// Normal vector at vertex (in model space)
in vec3 vNormal;

// Texture coordinate for this vertex
in vec2 vTexCoord;

uniform mat4 transform;
uniform mat4 camera;
uniform mat4 projection;

// OUTGOING DATA
varying vec3 vertex;
varying vec3 normals;
varying vec2 texCoord;
void main()
{

	mat4 modelview = camera * transform;
	
	texCoord = vTexCoord;
	vertex = vec3(modelview * vPosition);
	normals = normalize(transpose(inverse(mat3(modelview))) * vNormal);
    // Transform the vertex location into clip space
    gl_Position =  projection * camera  * transform * vPosition;
    //gl_Position = camera * transform * vPosition;
}
