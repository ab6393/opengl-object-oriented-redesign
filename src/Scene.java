
import java.util.ArrayList;
import java.util.Objects;
import javax.media.opengl.GL3;

/**
 * Scene.java
 * Contains all the objects and lights for rendering a scene. the heavy lifter of the 
 * project. stores all objects, lights, camera, and then renders them when asked
 * @author Andrew Baumher
 */
public class Scene
{
	public static final int MAX_LIGHTS = 8;

	Camera camera;
	ArrayList<Light> lights;
	ArrayList<Shape> shapes;

	
	public Scene()
	{
		lights = new ArrayList<>();
		shapes = new ArrayList<>();
		camera = Camera.DefaultCamera();
	}

	void PlaceCamera(Vertex pos, Vector up, Vertex lookAt)
	{
		camera.repositionCamera(pos, up, lookAt);
	}

	public void addLight(Light light)
	{
		if (lights.size() < MAX_LIGHTS)
		{
			lights.add(light);
		}
	}

	public void addShape(Shape shape)
	{
		shapes.add(shape);
	}

	public void render(GL3 gl3)
	{
		//render the shapes one at a time

		for (Shape shape : shapes)
		{
			int program = shape.shader;
			Obj src = shape.source;
			
			//sets up the model transforms
			//and the object material properties
			shape.SetupShape(gl3);
			//and now the view and projections
			camera.setupCamera(gl3, program);

			//and now we compound the lights and send them over
			//or at least we try. i originally packed all intensities into an array
			//and tried sending them that way. neither method works
			int index = 0;
			for (Light light : lights)
			{
				Color intensity = light.getIntensity();
				Magnitude attenuation = light.getAttenuation();
				Vec4 metadata = light.getAdditionalData();
				String loc = "lightIntensity["+ index +"]";
				int test = gl3.glGetUniformLocation(program, loc);
				gl3.glUniform4fv(test, 1, intensity.asArray(), 0);
				loc = "attenuation["+ index +"]";
				test = gl3.glGetUniformLocation(program, loc);
				gl3.glUniform3fv(test, 1, attenuation.asArray(), 0);
				loc = "lightData[" + index + "]";
				test = gl3.glGetUniformLocation(program, loc);
				gl3.glUniform4fv(test, 1, metadata.asArray(), 0);
				index++;
				
			}
			int shaderVar = gl3.glGetUniformLocation(program, "numLights");
			
			
			
			
			gl3.glUniform1i(shaderVar, lights.size());
			//sets up the object vertices and such
			src.drawObject(gl3, program);
		}
	}
}