
import com.jogamp.opengl.util.texture.Texture;
import javax.media.opengl.GL3;

/**
 * Shape.java
 * Contains the shapes and the materials that can be mapped to them
 * stores a reference to an object so multiple shapes can use the same object
 * data. could also do this with material, but not originally designed that way
 * @author Andrew Baumher
 */
public class Shape
{
	public Obj source;
	public int shader;
	Magnitude scale, translate, rotation;
	Material material;
	float ambience, diffuse, specular, shininess;

	public Shape(Shape other)
	{
		source = other.source;
		shader = other.shader;
		material = other.material;
		scale = other.scale;
		translate = other.translate;
		ambience = other.ambience;
		diffuse = other.diffuse;
		specular = other.specular;
		shininess = other.shininess;
	}

	Shape(Obj src, int program, Material mat, float amb, float dif, float spec, float shiny)
	{
		source = src;
		shader = program;
		material = mat;
		ambience = amb;
		diffuse = dif;
		specular = spec;
		shininess = shiny;
		scale = new Magnitude(1.0f, 1.0f, 1.0f);
		rotation = new Magnitude(0.0f, 0.0f, 0.0f);
		translate = new Magnitude(0.0f, 0.0f, 0.0f);
	}

	public final void TransformShape(float xScale, float yScale, float zScale,
		float xRot, float yRot, float zRot, float xTrans, float yTrans, float zTrans)
	{
		scale = new Magnitude(xScale, yScale, zScale);
		rotation = new Magnitude(xRot, yRot, zRot);
		translate = new Magnitude(xTrans, yTrans, zTrans);
	}

	final void TransformShape(Magnitude sc, Magnitude tr, Magnitude rot)
	{
		scale = sc;
		rotation = rot;
		translate = tr;
	}

	/**
	 * Set up this shape for drawing. Passes this specific instance of the
	 * object's ambience, diffuse, and specular coefficients.
	 *
	 * @param gl3 openGL gl3 object.
	 */
	public void SetupShape(GL3 gl3)
	{
		int program = shader;
		gl3.glUseProgram(program);
		SetupModel(gl3);
		material.SetupMaterial(program, gl3);
		int temp = gl3.glGetUniformLocation(program, "objAmbience");
		gl3.glUniform1f(temp, ambience);
		temp = gl3.glGetUniformLocation(program, "objDiffuse");
		gl3.glUniform1f(temp, diffuse);
		temp = gl3.glGetUniformLocation(program, "objSpecular");
		gl3.glUniform1f(temp, specular);
		temp = gl3.glGetUniformLocation(program, "objShininess");
		gl3.glUniform1f(temp, shininess);
	}

	/**
	 * Sets up the model transformations for drawing
	 * @param gl3 
	 */
	private void SetupModel(GL3 gl3)
	{
		int program = shader;
		float lx, ly, lz, tx, ty, tz;
		lx = scale.x;
		ly = scale.y;
		lz = scale.z;
		tx = translate.x;
		ty = translate.y;
		tz = translate.z;

		double cx, cy, cz, sx, sy, sz;
		double rx = Math.toRadians(rotation.x);
		double ry = Math.toRadians(rotation.y);
		double rz = Math.toRadians(rotation.z);
		cx = Math.cos(rx);
		cy = Math.cos(ry);
		cz = Math.cos(rz);
		sx = Math.sin(rx);
		sy = Math.sin(ry);
		sz = Math.sin(rz);
		//holy hell this is ugly. but correct. left as doubles to prevent data loss
		//until the last moment
		float[] modelTransform =
		{
			(float) (cy * cz * lx), (float) (cy * sz * ly), (float) (-sy * lz), tx,
			(float) ((sx * sy * cz - cx * sz) * lx), (float) ((sx * sy * sz + cx * cz) * ly),
			(float) (sx * cy * lz), ty,
			(float) ((cx * sy * cz + sx * sz) * lx), (float) ((cx * sy * sz - sx * cz) * ly),
			(float) ((cx * cy) * lz), tz,
			0.0f, 0.0f, 0.0f, 1.0f
		};

		gl3.glUseProgram(program);
		int modelLoc = gl3.glGetUniformLocation(program, "transform");
		gl3.glUniformMatrix4fv(modelLoc, 1, true, modelTransform, 0);
	}
}