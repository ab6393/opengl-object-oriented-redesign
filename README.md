# Object Oriented OpenGL Project
**Target Platform: All. Tested on Windows 7, 10**
As a final project for a Computer Graphics class, we were given free reign to do basically whatever we wanted, so long as it used everything we learned. My idea was to redesign the existing structure for our projects to a more object-friendly design. The projects had been written in C, then ported to C++, and finally ported again to JAVA, and as such didn't use many Object-Oriented practices. I decided to change that. While the end result was simple in scope, I had a lot of fun doing it, and decided to expand it. The goal of this project is to allow users to fully make use of the features of Object-Oriented designs: Changing a scene or adding a light or object is as simple as a single line of code. Further, if used in an educational setting, any part of this code could be removed and left for the students to implement, and it would take the educator little or no time to do so.  

## About

Using OpenGL at the low level means Vertex and Fragment files, so OpenGL can render 3D objects. I support Phongs and Simple Texture shaders, and 3D models stored in a WaveFront (.obj) format. For simplicity, animations (if any) must be stored and done outside of the files. The program renders Scenes, which are made of a Camera, zero or more Shapes, and zero to eight Lights. Shapes and Lights can be added to a scene via their respective add methods. The camera supports Orthographic or Frustum projections. Cameras are defined using a position, up vector, and target system.  

Effort was made to separate code to clear user-space and implementation-space, or a front-end and back-end. At it's barest level, the user only has to create a scene and run its render method to utilize the system in place. Users can then add .obj files to the Materials folder, and utilize them in their project by creating a new Obj, create a Shape from that Obj, and then add the Shape to the Scene. This also allows the user to use the same .obj multiple times, like a forest of trees that all use the same .obj file, for example.

## Compiling

Because OpenGL implementations are platform-dependant, you will need the right JAR files linked to your project. This project used JOGAMP, namely gluegen-rt and jogl. These can be found [here](https://jogamp.org/deployment/jogamp-current/archive/). I used an IDE for compilation, however, it is possible to do so with javac.
```sh
TODO: Figure out how to JAVAC this.
```

## Implementation

Lights, Objects, Shapes, Cameras, and Scenes are all defined in their own Class. A Shape is made of an Object and a Material. This seperation allows multiple shapes to use the same material, regardless of their object, and also build multiple shapes out of the same object. Shapes also have functions for changing their transform (scale, rotation, and/or position/translation). Note that the transform is calculated on the CPU once, instead of per vertex on the GPU/OpenGL implementation, for efficiency. Lights and Materials are abstract, and are implemented by their respective types. For example, a Directional Light and Ambient Light both implement light, but provide different functions. Lights, Shapes, Cameras, and Materials set their respective attributes in openGL (the frag and vert files) independant of one another, each through a function call. These functions are called just before each object is rendered, and is done so via the Scene's render method. 

## Usage

Rendering is done via JAVA's Frame class, with an instance of GLCanvas added in. As such, to use this, change the init function to whatever you desire. This project includes an example scene with a fake floor, fake skybox, a teapot, and a tree, which can be reused or discarded as desired. Documentation is available in the included Documentation.PDF.

## Disclaimer

Anyone who is using OpenGL instead of an API is likely attempting to reduce overhead and make their application run faster. As such, it makes little sense to use Object-Oriented Programming (let alone JAVA, where everything is an object); I am aware of this. This was not the goal. Instead, the goal is to showcase my understanding of OpenGL and provide a clean and elegant designer-friendly OpenGL tool, generally for educational or demonstrative use.  

## Example
Here is the provided example code, running on Windows 10.  
![Example Run](img/Run1.png)

## Further Work
Shadow Maps and Light Maps were out of the scope of the project. I have looked into implementing them, and may add them in at some point.
