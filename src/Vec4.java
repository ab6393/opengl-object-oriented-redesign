/**
 * Vec4.java
 * abstract vec4 class
 * @author Andrew Baumher
 */


public abstract class Vec4 
{
	
	public static final int NUM_ELEMS = 4;
	
	protected float x,y,z,w;
	public Vec4(float a, float b, float c, float d)
	{
		x = a;
		y = b;
		z = c;
		w = d;
	}
	
	public float[] asArray()
	{
		return new float[] {x, y, z, w};
	}
}

class Color extends Vec4
{
	public Color(float r, float g, float b, float a)
	{
		super(r,g,b,a);
	}
	
	public float Red()
	{
		return x;
	}
	
	public float Green()
	{
		return y;
	}
	
	public float Blue()
	{
		return z;
	}
	
	public float Alpha()
	{
		return w;
	}
}
