
/**
 * Light.java
 * An abstract class for all types of light.
 * @author Andrew Baumher
 */

public abstract class Light
{
	Color intensity;
	Magnitude attenuation;
	/**
	 * Constructor. sets a default attenuation of no attenuation
	 * @param r red intensity
	 * @param g green intensity
	 * @param b blue intensity
	 * @param a alpha intensity
	 */
	public Light(float r, float g, float b, float a)
	{
		intensity = new Color(r,g,b,a);
		attenuation = new Magnitude(1.0f, 0.0f, 0.0f);
	}
	/**
	 * Constructor as above but with attenuation
	 * @param r red intensity
	 * @param g green intensity
	 * @param b blue intensity
	 * @param a alpha intensity
	 * @param c constant attenuation
	 * @param l linear attenuation
	 * @param q quadratic attenuation
	 */
	public Light(float r, float g, float b, float a, float c, float l, float q)
	{
		intensity = new Color(r,g,b,a);
		
		attenuation = new Magnitude(c, l, q);
	}
	
	/**
	 * Returns the intensity wrapped in an array
	 * @return the intensity
	 */
	Color getIntensity()
	{
		return intensity;
	}
	
	/**
	 * Returns the attenuation wrapped in an array
	 * @return the attenuation
	 */
	Magnitude getAttenuation()
	{
		return attenuation;
	}
	/**
	 * Returns any additional data as a float array
	 * @return additional data
	 */
	public abstract Vec4 getAdditionalData();
}

/**
 * Ambient light implementation
 * @author Andrew Baumher
 */
class AmbientLight extends Light
{
	public AmbientLight(float r, float g, float b, float a)
	{
		super(r,g,b,a);
	}

	@Override
	public Vec4 getAdditionalData()
	{
		return new Color(0.0f, 0.0f, 0.0f, -1.0f);
	}
}

/**
 * Directional light implementation
 * @author Andrew Baumher
 */
class DirectionalLight extends Light
{
	Vector direction;

	//Has no decay, so no constructor for it
	public DirectionalLight(float r, float g, float b, float a, float x, float y, float z)
	{
		super(r, g, b, a);
		direction = new Vector(x,y,z);
	}
	public float[] getDirection()
	{
		return direction.asArray();
	}

	@Override
	public Vec4 getAdditionalData()
	{
		return new Color(direction.x, direction.y, direction.z, 0.0f);
	}
}

class PointLight extends Light
{
	Vertex position;
	public PointLight(float r, float g, float b, float a, 
		float c, float l, float q, float x, float y, float z)
	{
		super(r, g, b, a, c, l, q);
		position = new Vertex(x,y,z);
	}
	
	public float[] getPosition()
	{
		return position.asArray();
	}

	@Override
	public Vec4 getAdditionalData()
	{
		return new Color(position.x, position.y, position.z, 1.0f);
	}
}
