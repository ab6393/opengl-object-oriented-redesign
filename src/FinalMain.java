//
// textingMain.java
//
//  Created by Warren R. Carithers 2016/11/12.
//  Based on code created by Joe Geigel and updated by
//    Vasudev Prasad Bethamcherla.
//  Copyright 2016 Rochester Institute of Technology.  All rights reserved.
//
// Main class for lighting/shading/texturing assignment.
//
// This file should not be modified by students.
//

import java.awt.*;
import java.nio.*;
import java.awt.event.*;
import javax.media.opengl.*;
import javax.media.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FinalMain implements GLEventListener
{

	///
	// dimensions of the drawing window
	///
	private static final int WINDOW_WIDTH = 600;
	private static final int WINDOW_HEIGHT = 600;
	static GLCanvas myCanvas;
	Scene myScene;
	ShaderSetup shader;
	///
	// Initial animation rotation angles
	///

	///
	// Program IDs...for shader programs
	///
	public int pshader;
	public int tshader;

	///
	// canvas and shape info
	///
	Canvas canvas;
	private static Frame frame;

	///
	// Constructor
	///
	public FinalMain()
	{
		shader = ShaderSetup.getInstance();
		// Initialize lighting, view, etc.
		myScene = new Scene();
		//initialize the scene.
	}

	///
	// verify shader creation
	///
	private void checkShaderError(int program, String which)
	{
		if (program == 0)
		{
			System.err.println("Error setting " + which
				+ " shader - "
				+  shader.errorString(shader.shaderErrorCode)
			);
			System.exit(1);
		}
	}

	///
	// OpenGL initialization
	///
	@Override
	public void init(GLAutoDrawable drawable)
	{
		// get the gl object
		GL3 gl3 = drawable.getGL().getGL3();
		
		// Load texture image(s)
		tshader = shader.readAndCompile(gl3, "texture.vert", "texture.frag");
		checkShaderError(tshader, "texture");

		pshader = shader.readAndCompile(gl3, "phong.vert", "phong.frag");
		checkShaderError(pshader, "phong");
		
		myScene.PlaceCamera(new Vertex(2.0f, 2.0f, 1.0f), new Vector(0.0f, 1.0f, 0.0f), new Vertex(0.0f, 0.0f, -1.0f));
		myScene.camera.initializeProjection(-1.0f, 30.0f, -4.0f, 1.0f, 3.0f, -2.0f);
		myScene.camera.changeProjection(Camera.ProjectionType.ORTHOGRAPHIC);
		myScene.addLight(new AmbientLight(0.3f, 0.3f, 0.3f, 1.0f));
		myScene.addLight(new DirectionalLight(1.0f, 1.0f, 1.0f, 1.0f, -2.0f, 0.0f, 0.0f));
		Obj teapotObj = new Obj("teapot.obj");
		Obj treeObj = new Obj("tree.obj");
		Obj wallObj = new Obj("unitSquare.obj");
		Texture sky = null;
		int skyID = 0, treeID = 0;
		Texture tree = null;
		try
		{
			sky = TextureIO.newTexture(new File("Materials/skyTexture.png"), false);
			sky.setTexParameteri(gl3, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
			sky.setTexParameteri(gl3, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
			sky.setTexParameteri(gl3, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_EDGE);
			sky.setTexParameteri(gl3, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_EDGE);
			skyID = 0;
			tree = TextureIO.newTexture(new File("Materials/treeTexture.png"), false);
			tree.setTexParameteri(gl3, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
			tree.setTexParameteri(gl3, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
			tree.setTexParameteri(gl3, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_EDGE);
			tree.setTexParameteri(gl3, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_EDGE);
			treeID = 2;
		}
		catch (IOException | GLException ex)
		{
			Logger.getLogger(FinalMain.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}

		Shape teapot = generatePhongShape(teapotObj, new Color(1.0f, 0.8f, 0.2f, 1.0f),
			new Color(0.7f, 0.5f, 0.4f, 1.0f), new Color(0.6f, 0.5f, 0.7f, 1.0f), 
			0.4f, 0.4f, 0.7f, 3.0f);
		teapot.scale = new Magnitude (0.3f, 0.3f, 0.3f);
		teapot.translate = new Magnitude (0.0f, 1.0f, 0.0f);
		Shape tree1 = generateTexturedShape(treeObj, tree, treeID, true, 0.3f, 0.5f, 0.1f, 0.5f);
		tree1.scale = new Magnitude(0.4f, 0.4f, 0.4f);
		tree1.translate = new Magnitude(-1.0f, 0.5f, 1.0f);
		Shape floor = generatePhongShape(wallObj, new Color(0.023f, 0.972f, 0.011f, 1.0f), 
			new Color(0.0f, 0.3f, 0.1f, 1.0f), new Color(0.3f, 0.3f, 0.3f, 1.0f), 1.0f, 0.5f, 0.1f, 0.5f);
		
		floor.scale = new Magnitude(5.0f, 5.0f, 1.0f);
		floor.rotation = new Magnitude(90.0f, 0.0f, 00.0f);
		floor.translate = new Magnitude(0.0f, -0.5f, 0.5f);
		Shape xSky = generateTexturedShape(wallObj, sky, skyID, false, 0.3f, 0.5f, 0.1f, 0.5f);
		xSky.scale = new Magnitude(5.0f, 5.0f, 1.0f);
		xSky.rotation = new Magnitude(0.0f, 90.0f, 0.0f);
		xSky.translate = new Magnitude(-2.5f, 2.0f, 0.0f);
		Shape zSky = generateTexturedShape(wallObj, sky, skyID, false, 0.3f, 0.5f, 0.1f, 0.5f);
		zSky.translate = new Magnitude(0.0f, 2.5f, -2.5f);
		zSky.scale = new Magnitude(5.0f, 5.0f, 1.0f);
		myScene.addShape(teapot);
		myScene.addShape(tree1);
		myScene.addShape(xSky);
		myScene.addShape(zSky);
		myScene.addShape(floor);
		// Other GL initialization
		gl3.glEnable(GL.GL_DEPTH_TEST);
		gl3.glFrontFace(GL.GL_CCW);
		gl3.glPolygonMode(GL.GL_FRONT_AND_BACK, GL3.GL_FILL);
		gl3.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		gl3.glDepthFunc(GL.GL_LEQUAL);
		gl3.glClearDepth(1.0f);

	}

	///
	// Called by the drawable to initiate OpenGL rendering by the client.
	///
	@Override
	public void display(GLAutoDrawable drawable)
	{
		// get GL
		GL3 gl3 = (drawable.getGL()).getGL3();

		// clear and draw params..
		gl3.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		myScene.render(gl3);
	}

	///
	// Notifies the listener to perform the release of all OpenGL
	// resources per GLContext, such as memory buffers and GLSL
	// programs.
	///
	/**
	 *
	 * @param drawable
	 */
	@Override
	public void dispose(GLAutoDrawable drawable)
	{
	}

	///
	// Called by teh drawable during the first repaint after the component
	// has been resized.
	///
	/**
	 *
	 * @param drawable
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
		int height)
	{
	}

	///
	// main program
	///
	public static void main(String[] args)
	{
		// GL setup
		GLProfile glp = GLProfile.get(GLProfile.GL3);
		GLCapabilities caps = new GLCapabilities(glp);
		myCanvas = new GLCanvas(caps);

		// create your tessMain
		FinalMain myMain = new FinalMain();
		myCanvas.addGLEventListener(myMain);

		frame = new Frame("Final");
		frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		frame.add(myCanvas);
		frame.setVisible(true);

		//terminates the window.
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				frame.dispose();
				System.exit(0);
			}
		});
	}

	private Shape generateTexturedShape(Obj src, Texture texture, int texID, boolean affectedByLights,
		float ambience, float diffuse, float specular, float shininess)
	{
		return new Shape(src, tshader, new TexturedObj(texture, texID, affectedByLights),
			ambience, diffuse, specular, shininess);
	}
	
	private Shape generatePhongShape(Obj src, Color materialColor, 
		Color materialDiffuse, Color materialSpecular, float ambience, 
		float diffuse, float specular, float shininess)
	{
		return new Shape(src, pshader, new PhongShader(materialColor, 
			materialDiffuse, materialSpecular), ambience, diffuse, specular, shininess);
	}
}
