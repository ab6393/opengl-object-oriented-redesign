
import com.jogamp.opengl.util.texture.Texture;
import javax.media.opengl.GL3;

/**
 * Material.java
 * @author Andrew Baumher
 */

//likely should move to own file
/**
 * Material class. stores data on the material. different for all shader types
 * @author Andrew Baumher
 */
public abstract class Material
{
	public abstract void SetupMaterial(int program, GL3 gl3);
}

/**
 * Phong implementation of the material. has additional attributes that work with it
 * @author Andrew Baumher
 */
class PhongShader extends Material
{
	protected Texture texture;
	protected int textureID;
	protected boolean usesTexture;
	
	public Color color, diffuse, specular;
	public PhongShader(Color amb, Color dif, Color spec)
	{
		color = amb;
		diffuse = dif;
		specular = spec;
		texture = null;
		usesTexture = false;
	}

	public PhongShader(Texture tex, Color dif, Color spec)
	{
		color = null;
		texture = tex;
		diffuse = dif;
		specular = spec;
		usesTexture = true;
	}
	
	@Override
	public void SetupMaterial(int program, GL3 gl3)
	{
		gl3.glUseProgram(program);
		int temp = gl3.glGetUniformLocation(program, "matColor");
		gl3.glUniform4fv(temp, 1, color.asArray(), 0);
		temp = gl3.glGetUniformLocation(program, "matDiffuse");
		gl3.glUniform4fv(temp, 1, diffuse.asArray(), 0);
		temp = gl3.glGetUniformLocation(program, "matSpecular");
		gl3.glUniform4fv(temp, 1, specular.asArray(), 0);
	}
}

/**
 * And a textured object.
 * @author Andrew Baumher
 */
class TexturedObj extends Material
{
	protected Texture texture;
	protected int textureID;
	
	boolean affectedByLights;
	
	public TexturedObj(Texture tex, int TexID, boolean affected)
	{
		texture = tex;
		textureID = TexID;
		affectedByLights = affected;
	}

	@Override
	public void SetupMaterial(int program, GL3 gl3)
	{
		gl3.glUseProgram(program);
		int textureLoc = gl3.glGetUniformLocation(program, "tex");
		gl3.glActiveTexture(GL3.GL_TEXTURE0 + textureID);
		texture.enable(gl3);
		texture.bind(gl3);
		gl3.glUniform1i(textureLoc, textureID);
		int boolLoc = gl3.glGetUniformLocation(program, "affectedByLights");
		int passVal = (affectedByLights) ? 1 : 0;
		gl3.glUniform1i(boolLoc, passVal);
	}
}

