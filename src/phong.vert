#version 150

// Phong vertex shader
//
// Contributor: Andrew Baumher

// INCOMING DATA

// Vertex location (in model space)
in vec4 vPosition;

// Normal vector at vertex (in model space)
in vec3 vNormal;

uniform mat4 transform;
uniform mat4 camera;
uniform mat4 projection;

// OUTGOING DATA

//varying functions the same as out in this case
varying vec3 normals;
varying vec3 vertex;

void main()
{

    mat4 modelview = camera * transform;

	vertex = vec3(modelview * vPosition);

	normals = normalize(transpose(inverse(mat3(modelview))) * vNormal);

    // Transform the vertex location into clip space
    gl_Position = projection * camera  * transform * vPosition;
}